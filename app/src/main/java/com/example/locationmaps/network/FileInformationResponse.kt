package com.example.locationmaps.network

data class FileInformationResponse(var code: Int, var hasError: String, var valueResponse: String)