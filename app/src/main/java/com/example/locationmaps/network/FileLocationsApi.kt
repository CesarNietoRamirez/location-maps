package com.example.locationmaps.network

import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Url
import okhttp3.ResponseBody
import retrofit2.http.GET

interface FileLocationsApi {

    @FormUrlEncoded
    @POST("mob_sp_GetArchivo")
    fun getFileInformation(
        @Field("serviceId") serviceId: String,
        @Field("userId") userId: String
    ): Single<List<FileInformationResponse>>


    @GET
    fun DownloadFileZip(@Url fileUrl: String): Single<ResponseBody>

}