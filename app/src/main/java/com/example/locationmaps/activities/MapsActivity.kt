package com.example.locationmaps.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.example.locationmaps.R
import com.example.locationmaps.mvp.presenter.MapsPresenter
import com.example.locationmaps.mvp.view.MapsView

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    lateinit var mMap: GoogleMap
    lateinit var mapsPresenter: MapsPresenter
    lateinit var coordinateEt: EditText
    lateinit var drawMarkerBtn: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        coordinateEt = edit_text_coordinate
        drawMarkerBtn = button_marker

        val mapsView = MapsView(this)
        mapsPresenter = MapsPresenter(mapsView)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        drawMarkerBtn.isEnabled = true
        drawMarkerBtn.setOnClickListener { mapsPresenter.setLocation() }
    }
}
