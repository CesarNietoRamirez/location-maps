package com.example.locationmaps.activities

import android.os.Bundle
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.locationmaps.R
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_button.*
import java.util.concurrent.TimeUnit


class ButtonActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_button)

        val layoutDelay = layout_delay
        val delayBtn = Button(this)
        delayBtn.setText(R.string.delay_button)
        delayBtn.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )

        layoutDelay.addView(delayBtn)
        setContentView(layoutDelay)

        delayBtn.setOnClickListener {
            Completable.timer(10, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe { Toast.makeText(this, "Boton pulsado", Toast.LENGTH_SHORT).show() }
        }
    }
}
