package com.example.locationmaps.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.locationmaps.R
import com.example.locationmaps.mvp.presenter.LocationsWebServicePresenter
import com.example.locationmaps.mvp.view.LocationsWebServiceView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment

class LocationsWebServiceActivity : AppCompatActivity(), OnMapReadyCallback {

    lateinit var locationsWebServicePresenter: LocationsWebServicePresenter
    lateinit var mapLocationsRequest: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.location_service_activity_main)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map_locations_request) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val locationsWebServiceView = LocationsWebServiceView(this)
        locationsWebServicePresenter = LocationsWebServicePresenter(locationsWebServiceView)
    }

    override fun onMapReady(map: GoogleMap) {
        mapLocationsRequest = map
        locationsWebServicePresenter.getFileInformation()
    }
}
