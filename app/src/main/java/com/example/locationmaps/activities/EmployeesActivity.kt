package com.example.locationmaps.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.example.locationmaps.R
import com.example.locationmaps.mvp.model.realmDataBase.EmployeeRepository
import com.example.locationmaps.mvp.presenter.EmployeesPresenter
import com.example.locationmaps.mvp.view.EmployeesView
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_data_base.button_add
import kotlinx.android.synthetic.main.activity_data_base.recycler_view_employees

class EmployeesActivity : AppCompatActivity() {

    lateinit var employeesRecyclerView: RecyclerView
    lateinit var addEmployeeBtn: Button
    lateinit var employeesPresenter: EmployeesPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_base)

        employeesRecyclerView = recycler_view_employees
        addEmployeeBtn = button_add

        val employeesView = EmployeesView(this)
        val employeesRepository = EmployeeRepository(Realm.getDefaultInstance())
        employeesPresenter = EmployeesPresenter(employeesView, employeesRepository)
        employeesPresenter.fillInformation()

        addEmployeeBtn.setOnClickListener {
            employeesPresenter.addEmployee()
        }
    }

    override fun onResume() {
        super.onResume()
        employeesPresenter.showEmployees()
    }
}
