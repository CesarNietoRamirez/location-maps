package com.example.locationmaps.mvp.model.utils

import com.example.locationmaps.mvp.model.LocationObj

object RandomLocation {

    private val random = java.util.Random(System.currentTimeMillis())

    fun getRandomLocation() = LocationObj(
        (random.nextDouble() * 180 - 90),
        random.nextDouble() * 360 - 180
    )

}