package com.example.locationmaps.mvp.model.utils

import android.content.Context
import okhttp3.ResponseBody
import java.io.*
import java.util.zip.ZipFile


object FileUnzipUtils {

    private const val EMPTY_STRING = ""
    private const val LOCATIONS_FILE = "locations.zip"
    private val separator = File.separator


    fun writeResponseBodyZipToDisk(body: ResponseBody, context: Context): Boolean {
        try {
            val filePath = context.filesDir.path
            val fileSavedLocation = File("""$filePath$separator$LOCATIONS_FILE """)
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)
                var fileSizeDownloaded: Long = 0

                inputStream = body.byteStream()
                outputStream = FileOutputStream(fileSavedLocation)

                while (true) {
                    val read = inputStream?.read(fileReader)

                    if (read == -1) {
                        break
                    }
                    read?.let { outputStream.write(fileReader, 0, it) }

                    fileSizeDownloaded += read?.toLong() ?: 0

                }

                outputStream.flush()

                return true
            } catch (e: IOException) {
                return false
            } finally {
                inputStream?.close()

                outputStream?.close()
            }
        } catch (e: IOException) {
            return false
        }
    }

    fun unpackZipReadFile(context: Context): String {

        var strUnzipped = EMPTY_STRING
        val filePath = context.filesDir.path
        return try {
            val zipFile = ZipFile("""$filePath$separator$LOCATIONS_FILE """)
            val entries = zipFile.entries()
            while (entries.hasMoreElements()) {
                val entry = entries.nextElement()
                val stream = zipFile.getInputStream(entry)
                val bytes = ByteArray(entry.size.toInt())
                stream.read(bytes, 0, bytes.size)
                strUnzipped = String(bytes)
            }
            strUnzipped
        } catch (e: IOException) {
            e.printStackTrace()
            EMPTY_STRING
        }
    }
}