package com.example.locationmaps.mvp.model.JsonResponse

import com.google.gson.annotations.SerializedName

data class Ubicaciones(
    @SerializedName("FNLATITUD") var fnLatitude: Double,
    @SerializedName("FNLONGITUD") var fnLongitude: Double
)