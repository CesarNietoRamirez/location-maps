package com.example.locationmaps.mvp.model.realmDataBase

import io.realm.Realm
import io.realm.RealmResults

class EmployeeRepository(private val realmInstance: Realm) : IEmployeeRepository {

    override fun addEmployee(employeeEntity: EmployeesEntity) {
        realmInstance.executeTransaction {
            it.copyToRealmOrUpdate(employeeEntity)
        }
    }

    override fun deleteEmployeByName(nameEmployee: String) {
        realmInstance.executeTransaction {
            it.where(EmployeesEntity::class.java).equalTo("name", nameEmployee).findFirst()?.deleteFromRealm()
        }
    }

    override fun getAllEmployees(): RealmResults<EmployeesEntity>? {
        var allEmployees: RealmResults<EmployeesEntity>? = null
        realmInstance.executeTransaction {
            allEmployees = it.where(EmployeesEntity::class.java).findAll()

        }
        return allEmployees
    }

}