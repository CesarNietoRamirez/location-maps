package com.example.locationmaps.mvp.presenter

import com.example.locationmaps.mvp.model.utils.RandomLocation
import com.example.locationmaps.mvp.view.MapsView

class MapsPresenter(private val mapsView: MapsView){

    fun setLocation(){
        mapsView.showMarker(RandomLocation.getRandomLocation())
    }
}