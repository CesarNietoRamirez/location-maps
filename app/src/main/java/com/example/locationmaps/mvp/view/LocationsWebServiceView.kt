package com.example.locationmaps.mvp.view

import com.example.locationmaps.activities.LocationsWebServiceActivity
import com.example.locationmaps.mvp.model.LocationObj
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class LocationsWebServiceView(val locationsWebServiceActivity: LocationsWebServiceActivity) {

    fun showMarkers(locationObjs: List<LocationObj>) {

        locationObjs.forEach {
            val location = LatLng(it.latitude, it.longitude)
            locationsWebServiceActivity.mapLocationsRequest.addMarker(MarkerOptions().position(location))
            locationsWebServiceActivity.mapLocationsRequest.moveCamera(CameraUpdateFactory.newLatLng(location))
        }
    }
}