package com.example.locationmaps.mvp.model

import java.util.Date

data class EmployeeObj(var name: String, var dateOfBirth: Date, var rol: String)