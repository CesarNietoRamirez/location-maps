package com.example.locationmaps.mvp.model

data class LocationObj(val latitude: Double, val longitude: Double)