package com.example.locationmaps.mvp.model.realmDataBase

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class EmployeesEntity(@PrimaryKey var name: String = "", var dateOfBirth: Date = Date(), var rol: String = "") : RealmObject()