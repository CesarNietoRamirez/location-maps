package com.example.locationmaps.mvp.model.JsonResponse

import com.google.gson.annotations.SerializedName

data class ListaUbicaciones(
    @SerializedName("UBICACIONES") var ubicaciones: List<Ubicaciones>
)