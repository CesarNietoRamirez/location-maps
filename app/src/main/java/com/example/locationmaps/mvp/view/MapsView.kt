package com.example.locationmaps.mvp.view

import android.annotation.SuppressLint
import android.widget.Toast
import com.example.locationmaps.activities.MapsActivity
import com.example.locationmaps.mvp.model.LocationObj
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsView(private val mapsActivity: MapsActivity) {

    fun showMarker(locationObj: LocationObj) {
        val coordinateName = mapsActivity.coordinateEt.text.toString()
        if (coordinateName.isEmpty().not()) {
            val location = LatLng(locationObj.latitude, locationObj.longitude)
            mapsActivity.mMap.addMarker(MarkerOptions().position(location).title(coordinateName))
            mapsActivity.mMap.moveCamera(CameraUpdateFactory.newLatLng(location))
        } else {
            showErrorToast()
        }
    }

    @SuppressLint("ShowToast")
    fun showErrorToast() {
        Toast.makeText(mapsActivity, "Empty marker number", Toast.LENGTH_SHORT)
    }
}