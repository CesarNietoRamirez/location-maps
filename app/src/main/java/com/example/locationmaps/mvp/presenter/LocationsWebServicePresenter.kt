package com.example.locationmaps.mvp.presenter

import android.util.Log
import com.example.locationmaps.mvp.model.utils.FileUnzipUtils
import com.example.locationmaps.mvp.model.JsonResponse.ListaData
import com.example.locationmaps.mvp.model.LocationObj
import com.example.locationmaps.mvp.view.LocationsWebServiceView
import com.example.locationmaps.network.FileLocationsApi
import com.example.locationmaps.network.RetrofitServicesInstance
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LocationsWebServicePresenter(private val locationsWebServiceView: LocationsWebServiceView) {

    private lateinit var fileURL: String
    private val compositeDisposableLocations = CompositeDisposable()

    private var fileInformationApi: FileLocationsApi =
        RetrofitServicesInstance.getRetrofitInstance().create(FileLocationsApi::class.java)

    fun getFileInformation() {

        val fileInformation = fileInformationApi.getFileInformation(serviceId, userId)
        compositeDisposableLocations.add(
            fileInformation.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe({
                if (!it.isNullOrEmpty() && it[0].hasError.isNotEmpty() && it[0].hasError == "ok") {
                    it[0].valueResponse.let { urlFileZip ->
                        fileURL = urlFileZip
                        funDownloadZipFile()
                    }
                }
            }, {
                Log.e(LocationsWebServicePresenter::class.java.simpleName, it.message)
            })
        )
    }

    private fun funDownloadZipFile() {
        compositeDisposableLocations.add(
            fileInformationApi.DownloadFileZip(fileURL).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe({
                    FileUnzipUtils.writeResponseBodyZipToDisk(it, locationsWebServiceView.locationsWebServiceActivity)
                    getStringData()

                }, {
                    Log.e(LocationsWebServicePresenter::class.java.simpleName, it.message)
                })
        )
    }

    private fun getStringData() {
        val unpackZip = FileUnzipUtils.unpackZipReadFile(locationsWebServiceView.locationsWebServiceActivity)
        val gsonInstance = Gson()

        val fromJsonData = gsonInstance.fromJson(unpackZip, ListaData::class.java)
        val locationsMaps = fromJsonData.data.map {
            it.ubicaciones.flatMap { locations ->
                listOf(LocationObj(locations.fnLatitude, locations.fnLongitude))
            }
        }.flatten()
        locationsWebServiceView.showMarkers(locationsMaps)
    }

    companion object {
        const val serviceId = "55"
        const val userId = "12984"
    }
}