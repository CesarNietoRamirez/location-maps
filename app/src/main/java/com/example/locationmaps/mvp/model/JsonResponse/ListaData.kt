package com.example.locationmaps.mvp.model.JsonResponse

import com.google.gson.annotations.SerializedName

class ListaData(@SerializedName("data") var data: List<ListaUbicaciones>)