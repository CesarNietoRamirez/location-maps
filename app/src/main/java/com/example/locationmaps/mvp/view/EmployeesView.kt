package com.example.locationmaps.mvp.view

import androidx.recyclerview.widget.LinearLayoutManager
import com.example.locationmaps.activities.EmployeesActivity
import com.example.locationmaps.adapters.EmployeesAdapter
import com.example.locationmaps.mvp.model.EmployeeObj

class EmployeesView(private val employeesActivity: EmployeesActivity) {
    val employeesAdapter = EmployeesAdapter()
    fun showData(employees: List<EmployeeObj>) {

        employeesAdapter.employees = employees
        employeesActivity.employeesRecyclerView.adapter = employeesAdapter
        employeesActivity.employeesRecyclerView.layoutManager = LinearLayoutManager(employeesActivity)
        (employeesActivity.employeesRecyclerView.adapter as EmployeesAdapter).notifyDataSetChanged()
    }

    fun notifyDataChange(){
        (employeesActivity.employeesRecyclerView.adapter as EmployeesAdapter).notifyDataSetChanged()
    }

}