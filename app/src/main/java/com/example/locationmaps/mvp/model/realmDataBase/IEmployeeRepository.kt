package com.example.locationmaps.mvp.model.realmDataBase

import io.realm.RealmResults

interface IEmployeeRepository {
    fun addEmployee(employeeEntity: EmployeesEntity)
    fun deleteEmployeByName(nameEmployee: String)
    fun getAllEmployees(): RealmResults<EmployeesEntity>?
}