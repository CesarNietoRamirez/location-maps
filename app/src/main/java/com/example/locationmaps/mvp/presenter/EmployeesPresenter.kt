package com.example.locationmaps.mvp.presenter

import android.annotation.SuppressLint
import com.example.locationmaps.mvp.model.EmployeeObj
import com.example.locationmaps.mvp.model.realmDataBase.EmployeesEntity
import com.example.locationmaps.mvp.model.realmDataBase.IEmployeeRepository
import com.example.locationmaps.mvp.view.EmployeesView
import java.text.SimpleDateFormat
import java.util.Date

class EmployeesPresenter(private val employeesView: EmployeesView, private val employeesRepository: IEmployeeRepository) {

    private var employeesList: ArrayList<EmployeeObj>? = null

    init {
        val subscribe = employeesView.employeesAdapter.observableDelete.subscribe(
                {
                    deleteEmployee(it)
                }, {}, {})
    }

    fun fillInformation() {
        employeesRepository.getAllEmployees()?.let {
            if (it.isEmpty()) {
                getEmployees().forEach { employeesRepository.addEmployee(it) }
            }
        }
    }

    fun showEmployees() {

        employeesList = employeesRepository.getAllEmployees()?.map {
            EmployeeObj(it.name, it.dateOfBirth, it.rol)
        } as ArrayList<EmployeeObj>?

        employeesList?.let {
            employeesView.showData(it)
        } ?: employeesView.showData(mutableListOf(EmployeeObj("Not data found", Date(), "Not data found")))
    }

    fun addEmployee() {
        employeesList?.add(EmployeeObj("New Employee", Date(), "Desarrollador"))
        employeesRepository.addEmployee(EmployeesEntity("New Employee", Date(), "Desarrollador"))
        employeesView.notifyDataChange()
    }

    fun deleteEmployee(employee: EmployeeObj) {
        employeesList?.remove(employee)
        employeesRepository.deleteEmployeByName(employee.name)
        employeesView.notifyDataChange()
    }

    @SuppressLint("SimpleDateFormat")
    fun getEmployees(): List<EmployeesEntity> {
        val sdf = SimpleDateFormat("dd/MM/yyyy")

        val dateMiguel = sdf.parse("08/12/1990")
        val dateJuan = sdf.parse("03/06/1990")
        val dateRoberto = sdf.parse("14/10/1990")
        val dateCuevas = sdf.parse("08/12/1990")

        val listEmployees = mutableListOf<EmployeesEntity>()

        listEmployees.add(EmployeesEntity("Miguel Cervantes", dateMiguel, "Desarrollador"))
        listEmployees.add(EmployeesEntity("Juan Morales", dateJuan, "Desarrollador"))
        listEmployees.add(EmployeesEntity("Roberto Méndez", dateRoberto, "Desarrollador"))
        listEmployees.add(EmployeesEntity("Miguel Cuevas", dateCuevas, "Desarrollador"))

        return listEmployees
    }

}