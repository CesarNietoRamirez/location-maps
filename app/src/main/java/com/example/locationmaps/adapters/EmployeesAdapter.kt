package com.example.locationmaps.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.locationmaps.R
import com.example.locationmaps.mvp.model.EmployeeObj
import io.reactivex.subjects.PublishSubject

class EmployeesAdapter : RecyclerView.Adapter<EmployeesAdapter.EmployeesViewHolder>() {

    var employees: List<EmployeeObj> = mutableListOf()
    var observableDelete = PublishSubject.create<EmployeeObj>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.employee_item, parent, false)
        return EmployeesViewHolder(view)
    }

    override fun getItemCount() = employees.size

    override fun onBindViewHolder(holder: EmployeesViewHolder, position: Int) {
        holder.bindView(employees[position])
        holder.deleteImg.setOnClickListener {
            observableDelete.onNext(employees[position])
        }
    }

    class EmployeesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val nameTv = itemView.findViewById<TextView>(R.id.textView_name_employee)
        private val rolTv = itemView.findViewById<TextView>(R.id.textView_rol)
        val deleteImg= itemView.findViewById<ImageView>(R.id.imageButton_delete)
        fun bindView(employeeObj: EmployeeObj) {
            nameTv.text = employeeObj.name
            rolTv.text = employeeObj.rol
        }
    }
}